from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todo = TodoList.objects.all()
    context = {"todo_list": todo}
    return render(request, "todos/list.html", context)


def todo_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {"todo_details": details}
    return render(request, "todos/details.html", context)


def todo_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_update(request, id):
    list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=list)
    context = {
        "form": form,
        "list": list,
    }
    return render(request, "todos/edit.html", context)


def todo_delete(request, id):
    if request.method == "POST":
        todo = get_object_or_404(TodoList, id=id)
        todo.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "items/create.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.todo_details.id)
    else:
        form = TodoItemForm(instance=item)
    context = {
        "form": form,
    }
    return render(request, "items/update.html", context)
